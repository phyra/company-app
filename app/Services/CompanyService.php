<?php

declare(strict_types=1);

namespace App\Services;

use App\Http\Requests\CompanyRequest;
use App\Http\Requests\ValidationRequest;
use App\Interfaces\ServiceInterface;
use App\Models\Company;
use Illuminate\Database\Eloquent\Model;

class CompanyService implements ServiceInterface
{
    public function getModel(): Model
    {
        return new Company();
    }

    public function getValidationRequest(): ValidationRequest
    {
        return new CompanyRequest();
    }
}
