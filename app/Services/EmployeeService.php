<?php

declare(strict_types=1);

namespace App\Services;

use App\Http\Requests\EmployeeRequest;
use App\Http\Requests\ValidationRequest;
use App\Interfaces\ServiceInterface;
use App\Models\Employee;
use Illuminate\Database\Eloquent\Model;

class EmployeeService implements ServiceInterface
{
    public function getModel(): Model
    {
        return new Employee();
    }

    public function getValidationRequest(): ValidationRequest
    {
        return new EmployeeRequest();
    }
}
