<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Interfaces\ServiceInterface;
use App\Services\CompanyService;

class CompanyController extends ApiController
{
    public function getService(): ServiceInterface
    {
        return new CompanyService();
    }
}
