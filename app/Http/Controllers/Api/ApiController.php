<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use App\Interfaces\ApiControllerInterface;
use App\Interfaces\ServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

abstract class ApiController extends Controller implements ApiControllerInterface
{
    abstract public function getService(): ServiceInterface;

    public function index(): JsonResponse
    {
        return response()->json($this->getService()->getModel()->simplePaginate(10));
    }

    /**
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $validator = Validator::make(
            $request->all(),
            $this->getService()->getValidationRequest()->rules()
        );
        $validator->validate();

        $record = $this->getService()->getModel()->create($validator->validated());

        return response()->json($record, 201);
    }

    public function show($id): JsonResponse
    {
        $record = $this->getService()
            ->getModel()
            ->findOrFail($id);

        return response()->json($record);
    }

    /**
     * @throws ValidationException
     */
    public function update(Request $request, $id): JsonResponse
    {
        $record = $this->getService()
            ->getModel()
            ->findOrFail($id);

        $validator = Validator::make(
            $request->all(),
            $this->getService()
                ->getValidationRequest()
                ->rules()
        );
        $validator->validate();

        $record->update($validator->validated());

        return response()->json($record);
    }

    public function destroy($id): Response
    {
        $record = $this->getService()
            ->getModel()
            ->findOrFail($id);

        $record->delete();

        return response()->noContent();
    }
}
