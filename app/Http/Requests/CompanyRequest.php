<?php

namespace App\Http\Requests;

class CompanyRequest extends ValidationRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required',
            'nip' => 'required',
            'address' => 'required',
            'city' => 'required',
            'postal_code' => 'required'
        ];
    }
}
