<?php

namespace App\Http\Requests;

class EmployeeRequest extends ValidationRequest
{
    public function rules(): array
    {
        return [
            'company_id' => 'required|exists:companies,id',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone' => 'nullable',
        ];
    }
}
