<?php

declare(strict_types=1);

namespace App\Interfaces;

use App\Http\Requests\ValidationRequest;
use Illuminate\Database\Eloquent\Model;

interface ServiceInterface
{
    public function getModel(): Model;

    public function getValidationRequest(): ValidationRequest;
}
