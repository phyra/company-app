<?php

declare(strict_types=1);

namespace App\Interfaces;

use Illuminate\Http\Request;

interface ApiControllerInterface
{
    public function index();

    public function store(Request $request);

    public function show(int $id);

    public function update(Request $request, $id);

    public function destroy(int $id);
}
