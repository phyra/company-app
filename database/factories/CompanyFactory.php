<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    public function definition(): array
    {
        return [
            'name' => fake()->company,
            'nip' => random_int(10, 10),
            'address' => fake()->address,
            'city' => fake()->city,
            'postal_code' => fake()->postcode
        ];
    }
}
